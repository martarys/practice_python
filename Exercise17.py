# .This is the first 4-chili exercise of this blog!
# # # We’ll see what people think, and decide whether or not to
# # # continue with 4-chili exercises in the future.
# # # Exercise 17 (and Solution)
# # # Use the BeautifulSoup and requests Python packages to print
# # # out a list of all the article titles on the New York Times homepage

# import requests
# from bs4 import BeautifulSoup
#
# url = 'https://www.nytimes.com/'
# r = requests.get(url)
# # r_html = r.text
#
# soup = BeautifulSoup(r.text, 'html.parser')
#
# soup.find_all(class_="balancedHeadline")
#
# for heading in soup.find_all(class_="balancedHeadline"):
#     if heading.a:
#         print(heading.a.text)
#     else:
#         print(heading.contents[0])

import requests
from bs4 import BeautifulSoup

base_url = 'http://www.nytimes.com'
r = requests.get(base_url)
soup = BeautifulSoup(r.text, 'html.parser')

for story_heading in soup.find_all(class_="css-1cmu9py esl82me0"):
    if story_heading.a:
        print(story_heading.a.text.replace("\n", " ").strip())
    else:
        print(story_heading.contents[0].strip())