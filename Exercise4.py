# Create a program that asks the user for a number and then prints out a list of all
# the divisors of that number. (If you don’t know what a divisor is, it is a number
# that divides evenly into another number. For example, 13 is a divisor of 26
# because 26 / 13 has no remainder.)

num = int(input('Give a number: '))
div_list = []
for a in range (1, num):
    if num % a == 0:
        div_list.append(a)
print('The number:', num, 'divisors are: ', div_list)