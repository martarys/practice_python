# Generate a random number between 1 and 9 (including 1 and 9).
# Ask the user to guess the number, then tell them whether they guessed too low,
# too high, or exactly right. (Hint: remember to use the user input lessons from the very first exercise)
# Extras:
#
# Keep the game going until the user types “exit”
# Keep track of how many guesses the user has taken, and when the game ends, print this out.
import random


def guess_num():
    num = random.randint(1, 9)
    try:
        answer = int(input('Guess number from 1 to 9: '))
        while answer != num:
            if answer < num:
                answer = int(input('Your number is too small. Try again. \n'))
            else:
                answer = int(input('Your number is too big. Try again. \n'))
        print('Good job! It\'s ', num, '.')
        choice = (input('Wanna play again? Y/N'))
        choice = choice.upper()
        if choice == 'Y':
            guess_num()
        else:
            print('Bye')
            pass
    except:
        print('You should write a number')
        guess_num()

guess_num()
