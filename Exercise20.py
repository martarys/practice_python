# Write a function that takes an ordered list of numbers
# (a list where the elements are in order from smallest to largest) and another number.
# The function decides whether or not the given number is inside the list and returns
# (then prints) an appropriate boolean.
#
# Extras:
# Use binary search.
import random


a = random.choices(range(30), k=10)
a = list(set(sorted(a)))
print(a)

def search(ordered_list, number):
    while not number in ordered_list:
        print('The number ', number, ' is not in the list')
        return False
    print('The number ', number, ' is in the list')
    return True

print(search(a, 7))