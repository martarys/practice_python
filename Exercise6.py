# Ask the user for a string and print out whether this string is a palindrome or not.
# (A palindrome is a string that reads the same forwards and backwards.)

a = input('Write a word or short sentence using only letters and space: ')
raw_a = a.split(' ')
print(raw_a)
#TO Do - sprawdzić lower case

check_palindrome = ''
for word in raw_a:
    check_palindrome = check_palindrome + word
print(check_palindrome)

reverse = check_palindrome[::-1]
print(reverse)
if check_palindrome == reverse:
    print('You wrote a palindrome!')
else:
    print('This is not a palindrome')