# Write a program (using functions!) that asks the user for a
# long string containing multiple words. Print back to the user the same string,
# except with the words in backwards order. For example, say I type the string:
#   My name is Michele
# Then I would see the string:
#
#   Michele is name My
# shown back to me.
import random
def reverse_sentence():
    sentence = input('Write a sentence using few words: ')
    print('This is your sentence: ', sentence)
    a = sentence.split(' ')
    print(a)
    random.shuffle(a)
    a = ' '.join(a)
    print(a)

reverse_sentence()