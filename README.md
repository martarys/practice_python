**Practise Python **

My solutions of exercises from https://www.practicepython.org/
Exercises are in the “easy”, “medium”, and “hard” scale of difficulty.


* Technology 

Python 3.6


* Source 

PRACTICE PYTHON

https://www.practicepython.org/


