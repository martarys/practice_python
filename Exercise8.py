# Make a two-player Rock-Paper-Scissors game. (Hint: Ask for player plays (using input),
# compare them, print out a message of congratulations to the winner, and ask if the players want to start a new game)
# # Remember the rules: Rock beats scissors, Scissors beats paper, Paper beats rock
import random


def go():
    choice_player1 = int(input('Press "1" for rock, press "2" for scisors, press "3" for paper. '))
    player2 = random.randint(1, 4)

    if choice_player1 == 1:
        if player2 == 1:
            print('You have got rock, computer has rock. Let \'s play again. ')
            go()
        elif player2 == 2:
            print('You have got rock, computer has scisors. Rock beats scisors - You won!')
            next = input('Wanna play agaian? Y/N ').upper()
            if next == 'Y':
                go()
            else:
                print('Bye!')
                exit()
        else:
            print('You have got rock, computer has paper. Paper beats rock. You have lost. ')
            next = input('Wanna play agaian? Y/N ').upper()
            if next == 'Y':
                go()
            else:
                print('Bye!')
                exit()
    elif choice_player1 == 2:
        if player2 == 1:
            print('You have got scisors, computer has rock. Rock beats scisors, You have lost.')
            next = input('Wanna play agaian? Y/N ').upper()
            if next == 'Y':
                go()
            else:
                print('Bye!')
                exit()
        elif player2 == 2:
            print('You have got scisors, computer has scisors. Let\'s play again. ')
            go()
        else:
            print('You have got scisors, computer has paper. Scisors beats paper. You have won! ')
            next = input('Wanna play agaian? Y/N ').upper()
            if next == 'Y':
                go()
            else:
                print('Bye!')
                exit()
    elif choice_player1 == 3:
        if player2 == 1:
            print('You have got paper, computer has rock. Paper beats rock, You have won!')
            next = input('Wanna play agaian? Y/N ').upper()
            if next == 'Y':
                go()
            else:
                print('Bye!')
                exit()
        elif player2 == 2:
            print('You have got paper, computer has scisors. You have lost. ')
            next = input('Wanna play agaian? Y/N ').upper()
            if next == 'Y':
                go()
            else:
                print('Bye!')
                exit()
        else:
            print('You have got paper, computer has paper.  Let\'s play again. ')
            go()
    else:
        print('Invalid choice. Try again. ')
        go()


go()