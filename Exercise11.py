# Ask the user for a number and determine whether the number is prime or not.
# (For those who have forgotten, a prime number is a number that has no divisors.).
# You can (and should!) use your answer to Exercise 4 to help you.
# Take this opportunity to practice using functions, described below.

num = int(input('Give a number: '))
div_list = []
for a in range (1, num):
    if num % a == 0:
        div_list.append(a)
if len(div_list) == 2:
    print('The number ', num, ' is prime number')
else:
    print('The number:', num, 'is not a prime number')