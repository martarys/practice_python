# Write a program (function!) that takes a list and returns a new list
# that contains all the elements of the first list minus all the duplicates.
# Extras:
# Write two different functions to do this - one using a loop and constructing a list, and another using sets.
# Go back and do Exercise 5 using sets, and write the solution for that in a different function.

import random

a = random.choices(range(15), k=10)
print(a)

def remove_duplicates1(a):
    b = []
    for element in a:
        if not element in b:
            b.append(element)
    print(b)

def remove_duplicates2(a):
    b = list(set(a))
    print(b)

remove_duplicates1(a)
remove_duplicates2(a)