# Write a program that asks the user how many Fibonnaci numbers to generate and then generates them.
# Take this opportunity to think about how you can use functions. Make sure to ask the user to enter the
# number of numbers in the sequence to generate.(Hint: The Fibonnaci seqence is a sequence of numbers where
# the next number in the sequence is the sum of the previous two numbers in the sequence.
#     The sequence looks like this: 1, 1, 2, 3, 5, 8, 13, …)

def fibonacci_how_many():
    how_many = int(input('How many Fibonnaci numbers would you like to get? '))
    list = [0, 1]
    for i in range(0, how_many - 1):
        list.append(list[i] + list[i + 1])
    print(list[1:])


fibonacci_how_many()


def fibonacci_to_number():
    max = int(input('Give a range of your Fibonnaci serie '))
    x = 0
    y = 1
    sum = 1
    while sum <= max:
        print(sum)
        sum = x + y
        x = y
        y = sum



fibonacci_to_number()